# Learn C

## Basic Linux commands

1. Print working directory 
```shell script
pwd
```

2. List directory contents
```shell script
ls -l
```

3. Change directory
```shell script
cd /home
```

4. Compile c file
```shell script
gcc filename -o executable
```

5. Run executable file
```shell script
./filename
```
