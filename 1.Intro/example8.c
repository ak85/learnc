# include <stdio.h>

int main(void)
{
	int i=0;
	int j=0;
	int k=0;
	int number[3];
	int number_is_larger_than_10;
	int number_is_divided_by_3;

	for(i=0; i<3; i++){
		printf("Type a number: ");
		scanf("%d", &number[i]);
	}

	for(i=0; i<3; i++){
		number[i]=number[i]+1;
	}

	for(i=0; i<3; i++){
		number_is_larger_than_10 = number[i]>10;
		number_is_divided_by_3 = number[i] % 3 == 0;
		if(number_is_larger_than_10 == 0 && number_is_divided_by_3 == 1){
			printf("The number are: %d \n", number[i]);
		}
	}
}
